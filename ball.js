class Ball {
    constructor(artistin, xin, yin, din, idin, oin, total) {
      this.artist = artistin
      this.x = xin
      this.y = yin
      this.vx = 0
      this.vy = 0
      this.diameter = din
      this.radius = this.diameter / 2
      this.id = idin
      this.others = oin
      this.numBalls = total
      this.highlight = false
    }
  
    collide() {      
      for (let i = this.id + 1; i < this.numBalls; i++) {
        let dx = this.others[i].x - this.x
        let dy = this.others[i].y - this.y
        let distance = sqrt(dx * dx + dy * dy)
        let minDist = this.others[i].diameter / 2 + this.radius
        if (distance < minDist) {
          let angle = atan2(dy, dx)
          let targetX = width / 2
          let targetY = height / 2
          let ax = (targetX - this.others[i].x) * spring
          let ay = (targetY - this.others[i].y) * spring
          this.vx -= ax
          this.vy -= ay
          this.others[i].vx += ax
          this.others[i].vy += ay
        }
      }
    }
  
    move() {
      if (this.id === 0) return
      this.x += this.vx
      this.y += this.vy
      if (this.x + this.radius > width) {
        this.x = width - this.radius
        this.vx *= -1
      } else if (this.x - this.radius < 0) {
        this.x = this.radius
        this.vx *= -1
      }
      if (this.y + this.radius > height) {
        this.y = height - this.radius
        this.vy *= -1
      } else if (this.y - this.radius < 0) {
        this.y = this.radius
        this.vy *= -1
      }
    }
  
    display() {
      noStroke()
      if (this.highlight) {
        fill(255)
      } else {
        fill(255, 204)
      }
      ellipse(this.x, this.y, this.diameter, this.diameter)
      fill(0)
      textAlign(CENTER)
      text(this.artist.artistName + "\n" + this.artist.count, this.x, this.y)
    }

    contains(x, y) {
      let dx = x - this.x
      let dy = y - this.y
      let distance = sqrt(dx * dx + dy * dy)
      this.highlight = distance < (this.radius)
    }
  } 