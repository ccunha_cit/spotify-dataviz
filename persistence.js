let db

// Initialize database
(async function() {
  const sqlPromise = initSqlJs({
    locateFile: file => `./libraries/${file}`
  })
  const dataPromise = fetch("spotify.sqlite").then(res => res.arrayBuffer())
  const [SQL, buf] = await Promise.all([sqlPromise, dataPromise])
  db = new SQL.Database(new Uint8Array(buf))
})()

async function getTopArtists(total) {    
  let query = `SELECT DISTINCT count(*) as count, artistName FROM tracks GROUP BY artistName ORDER BY count DESC LIMIT ${total}`
  const stmt = db.prepare(query)
  let artists = []
  let totalPlays = 0
  while(stmt.step()) {
    let row = stmt.getAsObject()
    artists.push(row)
    totalPlays += row.count //row['count']
  }
  return { totalPlays, artists }
}

async function getArtistData(artistName) {
  let query = `SELECT trackName, msPlayed, endTime, timeStamp FROM tracks WHERE artistName = "${artistName}"`
  let tracks = []
  const stmt = db.prepare(query)
  while(stmt.step()) {
    let row = stmt.getAsObject()
    tracks.push(row)
  } 
  return tracks
}

async function getTopMusicsByArtist(artistName) {
  let query = `SELECT COUNT(*) as count, trackName timeStamp FROM tracks where artistName  = "${artistName}" GROUP BY trackName ORDER BY count DESC LIMIT 10`
  let tracks = []
  const stmt = db.prepare(query)
  while(stmt.step()) {
    let row = stmt.getAsObject()
    tracks.push(row)
  }  
  return tracks
}

async function get100TopMusics() {
  let query = `SELECT COUNT(*) as count, trackName, artistName timeStamp FROM tracks GROUP BY trackName ORDER BY count DESC LIMIT 100`
  let tracks = []
  const stmt = db.prepare(query)
  while(stmt.step()) {
    let row = stmt.getAsObject()
    tracks.push(row)
  }  
  return tracks
}

async function getTopArtistsByYearMonth() {
  let query = `SELECT COUNT(*) as count, artistName, STRFTIME('%Y-%m', date(t.endTime)) as monthdate FROM tracks t GROUP BY monthdate ORDER BY monthdate DESC`
  let artistName = []
  const stmt = db.prepare(query)
  while(stmt.step()) {
    let row = stmt.getAsObject()
    artistName.push(row)
  }  
  return artistName
}