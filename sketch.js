const total = 10
let spring = 0.001
let gravity = 0.03
let balls = []

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function(resolve, reject){
  setTimeout(resolve, 1000)
})

async function setup() {
  await delayToLoadDB
  createCanvas(1440, 800)
  const { totalPlays, artists } = await getTopArtists(total)

  console.log(artists[0].artistName)

  for (let i = 0; i < total; i++) {
    let diameter = map(artists[i].count, 0, totalPlays, 0, height)
    if (i > 0) {
      balls[i] = new Ball(artists[i], random(width), random(height), diameter, i, balls, total)
    } else {
      balls[i] = new Ball(artists[i], width/2, height/2, diameter, i, balls, total)
    }
  }

  await getArtistData(artists[0].artistName) 
}

function draw() {
  background(0)
  balls.forEach(ball => {
    ball.collide()
    ball.move()
    ball.display()
  })
}

function mouseMoved() {
  balls.forEach(ball => {
    ball.contains(mouseX, mouseY) 
  })
  return false
}