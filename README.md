Create database

% cd create-db
% node create.js

Information from 'all-streaming-history.json' will be read and a file named 'spotify.sqlite' created on project root folder.

---

Install browser-sync and run it to serve the contents

% npm install -g browser-sync
% browser-sync start --server -f -w
