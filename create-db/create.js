var fs = require("fs");
var initSqlJs = require('../libraries/sql-wasm.js');

const jsonData = JSON.parse(fs.readFileSync('all-streaming-history.json', 'utf8'));

initSqlJs().then(function(SQL) {
    var db = new SQL.Database();
    db.run("CREATE TABLE tracks (artistName, trackName, msPlayed, endTime, timeStamp);");
    
    for(let i in jsonData) {
        const artistName = jsonData[i].artistName;
        const trackName = jsonData[i].trackName;
        const msPlayed = jsonData[i].msPlayed;
        const endTime = jsonData[i].endTime;
        const timeStamp = new Date(endTime).getTime()
        db.run("INSERT INTO tracks VALUES (?,?,?,?,?)", [artistName,trackName,msPlayed,endTime,timeStamp]);
    }

    var data = db.export();
    var buffer = new Buffer.from(data);
    fs.writeFileSync("../spotify.sqlite", buffer);
} )